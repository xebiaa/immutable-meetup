docker build -t xebia/consul base
docker build -t xebia/consul-env consul-env
docker build -t xebia/consul-ui consul-ui
docker build -t xebia/consul-python consul-python
docker build -t xebia/consul-mongo consul-mongo